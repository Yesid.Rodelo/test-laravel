#!/usr/bin/env bash

# Add more custom commands here if needed
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo "$SERVER_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
~/.composer/vendor/bin/envoy run deploy --branch=$(printenv DEPLOYMENT_BRANCH)
cd /var/www/app/current
php artisan migrate --force
php artisan key:generate
php artisan storage:link

set -e

crontab -l | { cat; echo "* * * * * cd /var/www/app/current && php artisan schedule:run >> /dev/null 2>&1"; } | crontab -

exec supervisord -c /etc/supervisor/supervisord.conf
